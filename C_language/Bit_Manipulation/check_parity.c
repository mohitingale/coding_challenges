#include <stdio.h>
#include <stdbool.h>
 
unsigned findParity(unsigned n)
{
    bool parity = false;
    
    // run till n is not zero
    while (n)
    {
        // invert the parity flag
        parity = !parity;
 
        // turn off rightmost set bit
        n = n & (n - 1);
    }
    
    return parity;
}
 
// main function
int main()
{
    unsigned n = 31;
    
 
    if (findParity(n))
        printf("Parity of %d is odd", n);
    else 
        printf("Parity of %d is even", n);
}
  