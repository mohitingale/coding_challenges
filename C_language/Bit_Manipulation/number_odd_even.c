#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

bool num_is_odd(uint32_t num){
    return num&0x01;
}

int main(){
    if(num_is_odd(51)){
        printf("Number is odd\n");
    }
    if(!num_is_odd(50)){
        printf("Number is Even\n");
    }
}

