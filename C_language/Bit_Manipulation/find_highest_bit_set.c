#include <stdio.h>
#include <stdint.h>

uint32_t find_highest_bit(uint32_t num){
    for(int i=31;i>=0;--i){
        if(num & (1 << i)){
            return i;
        }
    }
    return 0;
}

int main(){
    uint32_t data = 159, highest_bit=0;
    highest_bit = find_highest_bit(data);
    printf("%d\n", highest_bit);
}