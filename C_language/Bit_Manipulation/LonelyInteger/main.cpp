//
//  main.cpp
//  LonelyInteger
//
//  Created by Mohit Ingale on 2/11/20.
//  Copyright © 2020 Mohit Ingale. All rights reserved.
//

#include <iostream>
#include <vector>

using namespace std;


int LonelyInteger(vector<int> input_array){
    int lonely_num = 0;
    for(auto each_num:input_array){
        lonely_num ^= each_num;
    }
    return lonely_num;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    vector<int> arr;
    arr.push_back(1);
    arr.push_back(2);
    arr.push_back(3);
    arr.push_back(2);
    arr.push_back(1);
    int lonely = LonelyInteger(arr);
    std::cout << "Hello, World!" << lonely << "\n";
    return 0;
}
