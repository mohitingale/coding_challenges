// Count number of setbits in 32 bit number in O(1)

#include <stdio.h>
#include <stdint.h>

#define B2(n) n, n+1, n+1, n+2
#define B4(n) B2(n), B2(n+1), B2(n+1), B2(n+2)
#define B6(n) B4(n), B4(n+1), B4(n+1), B4(n+2)

#define COUNT_BITS B6(0), B6(1), B6(1), B6(2)



uint8_t lookup_table[256] = {COUNT_BITS};

uint32_t num_of_bits_set(uint32_t num){
    uint32_t count = lookup_table[num & 0xff] + 
                    lookup_table[(num >> 8) &0xff] +
                    lookup_table[(num >> 16) &0xff] +
                    lookup_table[(num >> 24) &0xff];
    return count;
}

int main(){
    uint32_t n = 344;
    // for(uint32_t i=0;i<256;i++){
    //     printf("lookup_table[%d] = %d\n", i, lookup_table[i]);
    // }
    printf("Number of set bits = %d\n", num_of_bits_set(n));
}