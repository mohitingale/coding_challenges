// Given a sorted array arr[] and a number x, write a function that counts the occurrences of x in arr[]. (O(Log(N)))

#include <stdio.h>

//Brute Force
int find_occurrences_of_n_in_arr(int *arr_to_search, size_t arr_size, int num_to_search){
    int count = 0;
    for(size_t i=0; i<arr_size;i++){
        if(arr_to_search[i] == num_to_search){
            count++;
        }
    }
    return count;
}

//Binary Search
int find_adjacent_same_elements(int *array, size_t size, int num){
    int count = 0;
    size_t index = size/2;
    while((index<size) && (array[index++] == num)){
        count++;
    }
    index = size/2;
    while((index<size) && (array[index--] == num)){
        count++;
    }
    return count;
}

int find_occurrences_of_n_in_sorted_arr(int *arr_to_search, size_t arr_size, int num_to_search){
    if(arr_size <= 0){
        return 0;
    }
    size_t index = arr_size/2;
    if(arr_to_search[index] > num_to_search){
        return find_occurrences_of_n_in_sorted_arr(arr_to_search, arr_size/2, num_to_search);
    }
    else if(arr_to_search[index] < num_to_search){
        return find_occurrences_of_n_in_sorted_arr(arr_to_search + (arr_size/2), arr_size, num_to_search);
    }
    else{
        return find_adjacent_same_elements(arr_to_search, arr_size, num_to_search);
    }
}

int main(){
    int arr[10] = {2,5,5,5,6,6,7,8,9,11};
    printf("Occurences of 5 in arr = %d\n", find_occurrences_of_n_in_sorted_arr(arr, 10, 5));
}