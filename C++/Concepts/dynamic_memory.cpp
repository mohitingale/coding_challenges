//Illustrate copy constructor

#include <iostream>

class myint{
private:
    int *ptr;
    int len;
public:
    myint():ptr(NULL),len(0){}
    myint(int len):ptr(nullptr),len(len){
        ptr = new int[len];
    }
    // Copy Constructor
    myint(const myint &c_int){
        len = c_int.len;
        ptr = new int[len];
        for(int i=0;i<len;i++){
            ptr[i] = c_int.ptr[i];
        }
    }
    ~myint(){
        if(ptr!=nullptr){
            delete [] ptr;
        }
    }
};

int main(){
    myint num1(100);
    
}