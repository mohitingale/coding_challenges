/**
 * Example of ploymorphism
*/

#include <iostream>

#define VIRTUAL 1

using namespace std;

class printer{
public:
    //pure virtual
    // interface or abstract class
    virtual void pixel()=0;

    void draw_image(){
        cout << "Drawing Image ";
        pixel();
    }
};
/**
 * Model A and model b must implement pixel method else it will result in compilation error
**/
class model_a: public printer{
public:
    void pixel(){
        cout << "Advanced Pixel\n";
    }
};

class model_b: public printer{
public:
    void pixel(){
        cout << "Advanced Ultra Pixel\n";
    }
};

int main(){
    model_a a;
    model_b b;
    a.pixel();

    printer *ptr_a = &a;
    ptr_a->pixel();     //Prints model a pixel
    ptr_a->draw_image();

    printer *ptr_b = &b;
    ptr_b->pixel();     //Prints model a pixel
    ptr_b->draw_image();
}