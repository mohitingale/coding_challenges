/**
 * Example of ploymorphism
*/

#include <iostream>

#define VIRTUAL 1

using namespace std;

class printer{
public:
#if VIRTUAL
    virtual void pixel(){
#else
    void pixel(){
#endif
        cout << "basic pixel\n";
    }

    void draw_image(){
        cout << "Drawing Image ";
        pixel();
    }
};

class model_a: public printer{
public:
    void pixel(){
        cout << "Advanced Pixel\n";
    }
};

int main(){
    printer p;
    model_a a;
    p.pixel();
    a.pixel();
    printer *ptr_p = &p;
    ptr_p->pixel();
    ptr_p->draw_image();
    printer *ptr_a = &a;
    ptr_a->pixel();
    ptr_a->draw_image();
}