//
//  main.cpp
//  hamming_distance
//
//  Created by Mohit Ingale on 2/19/20.
//  Copyright © 2020 Mohit Ingale. All rights reserved.
//

#include <iostream>
#include <vector>

using namespace std;


int process_lane(int n, vector<int> start, vector<int>end){
    int t_size = n;
    int temp[t_size];
    for(int i=0;i<t_size;i++){
        temp[i] = 0;
    }
    
    for(auto data:start){
        temp[data] = 1;
    }
    for(auto data:end){
        temp[data] = 1;
    }
    int diff = 0;
    int prev_start = 0;
    for(int i = 0;i<t_size;i++){
        if(temp[i] == 1){
            int temp_diff = (i - prev_start)-1;
            if(temp_diff > diff){
                diff = temp_diff;
            }
            prev_start = i;
        }
    }
    return diff;
    
    
}



int find_ones_in_result(int data){
    int high_bit = 0;
    for(int i=0;i<(sizeof(data)*8);i++){
        if(data & (1<<i)){
            ++high_bit;
        }
    }
    return high_bit;
}
int hammingDistance(int x, int y) {
    int result = x ^ y;
    int distance = find_ones_in_result(result);
    return distance;
    
}

int main(int argc, const char * argv[]) {
//    int result = hammingDistance(10,45);,
    vector<int> start;
    start.push_back(1);
    start.push_back(2);
    vector <int> end;
    end.push_back(6);
    end.push_back(3);
    int data = process_lane(10, start,end);
    std::cout << data << std::endl;
    
    
    
    return 0;
}
